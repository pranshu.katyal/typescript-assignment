var Person = /** @class */ (function () {
    function Person(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    Person.quickSort = function (personArray, feild, orderOfSorting) {
        var tempArray = [].concat(personArray);
        if (tempArray.length == 1) {
            return tempArray;
        }
        var pivot = tempArray[0][feild];
        var pivotItems = tempArray[0];
        var left = [];
        var right = [];
        for (var i = 0; i < tempArray.length; i++) {
            if (orderOfSorting == 'asc') {
                tempArray[i][feild] < pivot ? left.push(tempArray[i]) : right.push(tempArray[i]);
            }
            else {
                tempArray[i][feild] < pivot ? right.push(tempArray[i]) : left.push(tempArray[i]);
            }
        }
        return Person.quickSort(left, feild, orderOfSorting).concat(pivotItems, Person.quickSort(right, feild, orderOfSorting));
    };
    return Person;
}());
var Person1 = new Person('pranshu', 21, 4000, 'M');
var Person2 = new Person('shrey', 22, 4000, 'M');
var Person3 = new Person('abhishek', 11, 3000, 'M');
var Person4 = new Person('nitya', 8, 8000, 'F');
var personArray = [Person1, Person2, Person3, Person4];
console.log(Person.quickSort(personArray, 'age', 'asc'));
