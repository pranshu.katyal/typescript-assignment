class Person{
    name:string;
    age:number;
    salary:number;
    sex:string;

    constructor(name:string,age:number,salary:number,sex:string){
        this.name=name;
        this.age=age;
        this.salary=salary;
        this.sex=sex;

    }
    static quickSort(personArray:Person[],feild:string,orderOfSorting:string):Person[]
    {
        let tempArray:Person[]=[].concat(personArray);
        if(tempArray.length==1){
            return tempArray;
        }
        let pivot:number|string=tempArray[0][feild];
        let pivotItems:Person=tempArray[0];

        let left:Person[]=[];
        let right:Person[]=[];

        for(let i=0;i<tempArray.length;i++){
            if(orderOfSorting=='asc'){
                tempArray[i][feild]<pivot?left.push(tempArray[i]):right.push(tempArray[i]);
            }else{
                tempArray[i][feild]<pivot?right.push(tempArray[i]):left.push(tempArray[i]);
            }
        }
        return Person.quickSort(left,feild,orderOfSorting).concat(pivotItems,Person.quickSort(right,feild,orderOfSorting));
    }
}

let Person1:Person=new Person('pranshu',21,4000,'M');
let Person2:Person=new Person('shrey',22,4000,'M');
let Person3:Person=new Person('abhishek',11,3000,'M');
let Person4:Person=new Person('nitya',8,8000,'F');
const personArray:Person[]=[Person1,Person2,Person3,Person4];
console.log(Person.quickSort(personArray,'age','asc'));